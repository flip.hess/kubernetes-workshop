# Devops Kubernetes Workshop

This repository contains a very simple python application that returns some output.

Your goal is to improve this application to be able to run in kubernetes.

Don't let the python/backend part of the code hold you back: We won't be making any code changes for
the main part of this workshop: In the bonus assigments there is time to improve
the code and optimise and secure this minimal application if you feel like it.

## Deliverables

- New files and modifications to the application to be able to run in Minikube (Linux) or Docker desktop for Mac (macOS)
- BONUS: configure GitLab CI

Before you start working, please fork this repository to your own personal gitlab: We're going to run this later.


## Stage 1 - Run the application locally to verify it's behaviour 

```
> pip install -r requirements.txt

> FLASK_APP=app.py flask run -h 0.0.0.0 -p 3001
```

Once it's running you can test the endpoints with
```
> curl -L http://localhost:3001/
```


## Stage 2 - Create a docker container for this application, so we can run our app in a local kubernetes cluster

We're going to containerize this small app. 
Please create a dockerfile that can be started without extra command line arguments that runs the app. 

You can start with something like:

```
FROM python:3.10
<insert your steps here>
```

**Note** There is no need to use `uwsgi` and other additional application handlers: The
developer service as mentioned in step 1 is good enough for this assignment.


After creating the dockerfile, you should be able to run your app with: 
```
docker build -t app . 
docker run -ti app 
```

## Stage 3 - Create a helm chart for this application

Now that we have a docker container, we can prepare for creating a helm chart
for it. 

Helm provides a very nice basic skeletton we can use for creating our initial
helm chart: 

```
cd kubernetes-workshop
mkdir -p charts
helm create charts/kubernetes-workshop
```

In the charts directory, we now have a basic helm chart we can adjust to make it
run in kubernetes. 

You can test if your yaml templates are working properly using:
```
helm template kubernetes-workshop charts/kubernetes-workshop --debug
```

To install or upgrade the helm chart locally on your kubernetes cluster you can use: 
```
helm upgrade --install kubernetes-workshop charts/kubernetes-workshop
```

This will install your helm chart application in the `default` namespace, so to
check the status of your pods, you can do this with kubectl: 

```
kubectl get pods -n default
```


## Stage 4 - Bonus - Optimizations 

We can add some additional improvements in our helm chart. 

For this we need the utility `kube-score` to find the best practises for
production ready helm charts.

Please download it here: https://github.com/zegl/kube-score/releases

You can run kube-score using helm template:

```
helm template kubernetes-workshop charts/kubernetes-workshop | kube-score -
```

This will list several suggestions like: 

- Using enough replicas to run on spot instances
- Setting a podDisruption budget 
- Setup affinity and tolerations 
- Setting a Security context for both pods and containers 
- Configuring a readonly root filesystem
- Have a correct ingress object to make this service accessible over DNS
- Create an apparmor or seccomp profile 
- Implement container signing 


Implement the suggestions to make `kube-score` succeed without errors or
suggestions.


## Stage 5 - Superbonus  - Code improvements and Optimizations

In the python app are some security issues and bad practises that are not
considered the right way to do it: 

Please create a list of improvements and / or add the required code changes to make
this application work in a production environment 

**Notes:**

- Think about logging and monitoring. Suggest and implement improvement.
- Security is our priority. What should be improved or changed? If you can, implement the changes.


## Stage 6 - Forever glory and poof you are now a shadow platform engineer - Deployment on CI 

Using our CI templates, it is possible to deploy this app in our CI cluster:
If you know how to do this, implement our CI template for helm
(`/templates/helm.yml`) in such a way that do can deploy this app to our ci cluster 


**Note:** You might experience some issues running a personal project on our CI gitlab
runners: To make this work, create a demo repository in the tiqets group to
ensure your pipeline is able to run on our self-hosted gitlab runners. 

